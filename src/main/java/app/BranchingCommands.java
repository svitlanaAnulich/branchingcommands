package app;

import app.statistic.Statistic;
import app.statistic.TimeWorkingStatistic;
import app.utils.ApachePOIExcelWrite;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

public class BranchingCommands {

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, IOException {

        System.out.println("Start Branching Commands program");

        Statistic statistic = new Statistic();

        List<Integer> arraySizeList = Arrays.asList(100,200,300,400,500,600,700,800,900,1000);

        List<TimeWorkingStatistic> listTimes = statistic.create(arraySizeList, Integer.class);


        ApachePOIExcelWrite apacheWrite = new ApachePOIExcelWrite();
        String fileName = "SortArrayStatistic.xlsx";
        apacheWrite.write(new File(fileName), listTimes);
        apacheWrite.createCharts(fileName);

    }
}
