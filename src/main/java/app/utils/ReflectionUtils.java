package app.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;


public class ReflectionUtils {

    /**
     * Find methods from Class with annotation
     *
     * @param clazz      - class for finding
     * @param annotation - annotation with finding
     * @return - list of methods
     */
    public List<Method> getListAnnotatedMethods(Class<?> clazz, Class<? extends Annotation> annotation) {
        Method[] methods = clazz.getDeclaredMethods();
        List<Method> annotatedMethods = new ArrayList<Method>(methods.length);
        for (Method method : methods) {
            if (method.isAnnotationPresent(annotation)) {
                annotatedMethods.add(method);
            }

        }
        return annotatedMethods;
    }

    /**
     * Method witch calculate working time of another method
     *
     * @param method - method witch working time we calculate
     * @param args   - ars for method with working time we calculate
     * @return - time for working
     */
    public long getMethodWorkingTime(Method method, Object args) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        long startTime, endTime;
        startTime = System.nanoTime();

        runMethod(method, args);

        endTime = System.nanoTime();
        return endTime - startTime;
    }

    /**
     * Run method
     *
     * @param method - method witch run
     * @param args   - arguments for method with run
     */
    public Object runMethod(Method method, Object args) throws ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Class<?> c = Class.forName(method.getDeclaringClass().getName());
        return runMethod(method, args, runConstructor(c, null));
    }

    /**
     * Run method and get Instance with arguments
     *
     * @param method      - method with run
     * @param args        - arguments for method
     * @param constructor - constructor instance
     * @return - result of working method with run
     */
    public Object runMethod(Method method, Object args, Object constructor) throws InvocationTargetException, IllegalAccessException {
        method.setAccessible(true);
        try {
            return method.invoke(constructor);
        } catch (IllegalArgumentException e){
            return method.invoke(constructor, args);
        }
    }

    public Object runConstructor(Class<?> clazz, Object... arg) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Class<?> c = Class.forName(clazz.getName());
        try {
            return c.newInstance();
        } catch (InstantiationException e){
            Constructor<?> constructor = c.getConstructor(getClassForArgs(arg));
            return constructor.newInstance(arg);
        }
    }

    private Class<?>[] getClassForArgs(Object[] arg) {
        Class[] cls = new Class[arg.length];
        for (int i = 0; i < arg.length; i++) {
            cls[i] = arg[i].getClass();
        }
        return cls;
    }

    public int getCountMethods(Class<?> clazz, Class<? extends Annotation> annotation) {
        List<Method> list = getListAnnotatedMethods(clazz,annotation);
        return list.size();
    }
}
