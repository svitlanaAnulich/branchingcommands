package app.utils;

import app.statistic.TimeWorkingStatistic;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.charts.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.xssf.usermodel.charts.XSSFChartLegend;

import java.io.*;
import java.util.List;


/**
 * Excel writer with charter drawing
 */
public class ApachePOIExcelWrite {

    /**
     * Create Excel file and write there app.arrays.statistic data
     *
     * @param fileName   - excel file
     * @param statistics - list of timeWorkingStatistic
     * @throws IOException
     */
    public void write(File fileName, List<TimeWorkingStatistic> statistics) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(fileName);
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = null;
        int rowNum = 0, cellNum =1;
        Row row;

        for (int i = 0; i < statistics.size(); i++) {
            if (i == 0) {
                sheet = workbook.createSheet("Sheet" + statistics.get(0).getArrayCreationName());
            } else {
                if (!statistics.get(i - 1).getArrayCreationName().equals(statistics.get(i).getArrayCreationName())) {
                    sheet = workbook.createSheet("Sheet" + statistics.get(i).getArrayCreationName());
                    rowNum = 0;
                    cellNum =0;
                }
                if (!statistics.get(i - 1).getSortingMethodName().equals(statistics.get(i).getSortingMethodName())) {
                    rowNum = 0;
                    cellNum++;
                }
            }

            if (cellNum==1){
                row = sheet.createRow(rowNum++);
            } else {
                row = sheet.getRow(rowNum++);
            }
            if (rowNum == 1) {
                Cell cell = row.createCell(0);
                cell.setCellValue("Array Size");
                cell = row.createCell(cellNum);
                cell.setCellValue(statistics.get(i).getSortingMethodName());
            } else {
                Cell cell = row.createCell(0);
                cell.setCellValue(statistics.get(i).getArraySize());
                cell = row.createCell(cellNum);
                cell.setCellValue(statistics.get(i).getWorkingTime());
            }
        }

        workbook.write(outputStream);
        workbook.close();
        outputStream.close();
    }

    /**
     * Write charts list in Excel file
     * @param fileName - excel file
     * @throws IOException
     */
    public void createCharts(String fileName) throws IOException {
        FileInputStream file_input = new FileInputStream(new File(fileName));
        XSSFWorkbook workbook = new XSSFWorkbook(file_input);
        int sheetsCount = workbook.getNumberOfSheets();


        for (int i = 0; i < sheetsCount; i++) {
            XSSFSheet sheet = workbook.getSheetAt(i);
            int rowCount = sheet.getLastRowNum()+2;
            Area showChartArea = new Area(rowCount,1,rowCount+15,10);
            createBarChart(sheet, showChartArea);
        }

        FileOutputStream fileOut = new FileOutputStream(fileName);
        workbook.write(fileOut);
        fileOut.close();

    }

    /**
     * Add One chart to the Excel file
     *
     * @param sheet - sheet of excel file
     * @throws IOException
     */
    public void createBarChart(XSSFSheet sheet, Area showChartArea) throws IOException {

        //chart
        XSSFDrawing xlsx_drawing = sheet.createDrawingPatriarch();
        XSSFClientAnchor anchor = xlsx_drawing.createAnchor(0, 0, 0, 0,
                showChartArea.leftCol,
                showChartArea.leftRow,
                showChartArea.rightCol,
                showChartArea.rightRow);

        XSSFChart lineChart = xlsx_drawing.createChart(anchor);
        XSSFChartLegend legend = lineChart.getOrCreateLegend();
        legend.setPosition(LegendPosition.BOTTOM);
        LineChartData data = lineChart.getChartDataFactory().createLineChartData();
        ChartAxis bottomAxis = lineChart.getChartAxisFactory().createCategoryAxis(AxisPosition.BOTTOM);
        ValueAxis leftAxis = lineChart.getChartAxisFactory().createValueAxis(AxisPosition.LEFT);
        leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);

        // data for chart
        ChartDataSource<Number> xs = DataSources.fromNumericCellRange(sheet,
                new CellRangeAddress(1,
                        sheet.getLastRowNum()+1,
                        0,
                        0));
        int countColum = sheet.getRow(0).getLastCellNum()-1;
        for (int i = 0; i < countColum; i++) {
            ChartDataSource<Number> ys = DataSources.fromNumericCellRange(sheet,
                    new CellRangeAddress(1,
                            sheet.getLastRowNum()+1,
                            i+1,
                            i+1));
            LineChartSeries chartSerie = data.addSeries(xs, ys);
            String string = sheet.getRow(0).getCell(i+1).toString();
            chartSerie.setTitle(string);
        }

        //draw
        lineChart.plot(data, bottomAxis, leftAxis);

    }

    /**
     * Class using for drawing Area
     */
    public class Area{

        public int leftRow;
        public int leftCol;
        public int rightRow;
        public int rightCol;

        public Area(int leftRow, int leftCol, int rightRow, int rightCol) {
            this.leftRow = leftRow;
            this.leftCol = leftCol;
            this.rightRow = rightRow;
            this.rightCol = rightCol;
        }
    }
}
