package app.utils;

import java.io.File;

public class FileUtils {

    public File findFile(String fileName, String rootDir) {
        File folder = new File(rootDir);
        if (!folder.isDirectory()) throw new IllegalArgumentException(rootDir+ " must be a directory");
        return findFileRecursively(fileName, rootDir);
    }

    private File findFileRecursively(String fileName, String rootDir){
        File file = new File(rootDir);
        if (file.isDirectory()) {
            File[] arr = file.listFiles();
            for (File f : arr) {
                File found = findFileRecursively(fileName, f.getAbsolutePath());
                if (found != null)
                    return found;
            }
        } else {
            if (file.getName().equals(fileName)) {
                return file;
            }
        }
        return null;
    }

}
