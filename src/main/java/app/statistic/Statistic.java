package app.statistic;

import app.arrays.create.ArrayFilling;
import app.arrays.create.FillArray;
import app.arrays.sort.MySorting;
import app.arrays.sort.SortMethod;
import app.utils.ReflectionUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Class create statistic working time of sorting methods
 */
public class Statistic {

    /**
     * Create List with satiating of working time sorting methods from {@link MySorting}
     * for arrays from {@link FillArray} with {@link ArrayFilling} annotation
     * @param arraySizeList - list size of arrays
     * @param clazz - type of Numbers at arrays
     * @return - list of working time statistic
     */
    public List<TimeWorkingStatistic> create(List<Integer> arraySizeList, Class clazz) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        List<TimeWorkingStatistic> returnList = new ArrayList<TimeWorkingStatistic>();
        ReflectionUtils reflectionUtils = new ReflectionUtils();
        MySorting mySorting = new MySorting();
        List<Method> listSortMethods = mySorting.getListSortMethods();
        List<Method> listCreateArraysMethods = reflectionUtils.getListAnnotatedMethods(FillArray.class, ArrayFilling.class);

        for (Method getArrayMethod : listCreateArraysMethods) {
            for (Method sortMethod : listSortMethods) {

                for (Integer arraySize : arraySizeList) {

                    Object arglist[] = new Object[2];
                    arglist[0] = arraySize;
                    arglist[1] = clazz;
                    Object constructor = reflectionUtils.runConstructor(FillArray.class, arglist);

                    Number[] array = (Number[]) reflectionUtils.runMethod(getArrayMethod, null, constructor);

                    long time = reflectionUtils.getMethodWorkingTime(sortMethod, array);
                    TimeWorkingStatistic timeWorkingStatistic = new TimeWorkingStatistic(
                            arraySize,
                            sortMethod.getAnnotation(SortMethod.class).name(),
                            getArrayMethod.getAnnotation(ArrayFilling.class).name(),
                            time
                    );
                    returnList.add(timeWorkingStatistic);

                }
            }
        }

        return returnList;
    }
}
