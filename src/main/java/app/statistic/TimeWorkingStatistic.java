package app.statistic;

/**
 * Class with variables with statistic for one point
 */

public class TimeWorkingStatistic {

    private Long arraySize;
    private String sortingMethodName;
    private String arrayCreationName;
    private Long workingTime;

    public TimeWorkingStatistic(long arraySize, String sortingMethodName, String arrayCreationName, long workingTime) {
        this.arraySize = arraySize;
        this.sortingMethodName = sortingMethodName;
        this.arrayCreationName = arrayCreationName;
        this.workingTime = workingTime;
    }

    @Override
    public String toString(){
        String returnString = "[ "+arraySize + ", ";
        returnString  += sortingMethodName + ", ";
        returnString += arrayCreationName + ", ";
        returnString += workingTime + " ]";
        return  returnString;
    }

    public long getArraySize() {
        return arraySize;
    }

    public String getSortingMethodName() {
        return sortingMethodName;
    }

    public String getArrayCreationName() {
        return arrayCreationName;
    }

    public long getWorkingTime() {
        return workingTime;
    }
}
