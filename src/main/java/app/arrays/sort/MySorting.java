package app.arrays.sort;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

/**
 * Generic Class for add methods for sorting Array with Numbers
 *
 * @param <T>
 */

public class MySorting<T extends Number> extends Sorting<T> {

    /**
     * Sorting method from {@link Arrays} class
     * @param array - not sorted array
     * @return - sorted array
     */
    @SortMethod(name = "Java Arrays sort")
    public T[] arraysSort(T[] array) {
        Arrays.sort(array);
        return array;
    }

    /**
     * Get list of sorting method from this class and it super class
     * @return - list methods with @SortMethod annotation
     */
    public List<Method> getListSortMethods(){
        return getListSortMethods(getClass());
    }

}
