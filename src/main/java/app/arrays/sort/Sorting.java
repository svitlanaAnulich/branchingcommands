package app.arrays.sort;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Generic Class contains methods for sorting Array with Numbers
 *
 * @param <T>
 */
public class Sorting<T extends Number> implements Comparator<T>{

    /**
     * start method for quick sort
     *
     * @param array - Not sorted array with numbers
     * @return - sorted array
     */
    @SortMethod(name = "Quick sort")
    protected T[] quickSort(T[] array) {
        quickSortRecursion(0, array.length - 1, array);
        return array;
    }

    /**
     * quick sorting algorithm
     * @param low
     * @param high
     * @param array
     */
    private void quickSortRecursion(int low, int high, T[] array) {
        int i = low, j = high;
        T mainElement = array[low + (high - low) / 2];

        while (i <= j) {
            while (compare(array[i], mainElement) < 0) {
                i++;
            }
            while (compare(array[j], mainElement) > 0) {
                j--;
            }
            if (i <= j) {
                T tmpElement = array[i];
                array[i] = array[j];
                array[j] = tmpElement;
                i++;
                j--;
            }
        }
        // Recursion
        if (low < j)
            quickSortRecursion(low, j, array);
        if (i < high)
            quickSortRecursion(i, high, array);

    }

    /**
     * insertion sorting algorithm
     *
     * @param array - Not sorted array with numbers
     * @return - sorted array
     */
    @SortMethod(name = "Insertion Sort")
    protected T[] insertionSorting(T[] array) {

        for (int i = 0; i < array.length; i++) {
            T tempElement = array[i];
            int j = i - 1;
            while (j >= 0 && compare(array[j], tempElement) > 0) {
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = tempElement;
        }
        return array;
    }

    /**
     * start method for bubble sort DESC algorithm
     * @param array - not sorted array with numbers
     * @return - sorted array
     */
    @SortMethod(name = "Bubble DESC Sort")
    protected T[] descBubbleSort(T[] array) {
        return sortArrayByBubble(array, SortDirection.DESC);
    }

    /**
     * start method for bubble sort ASC algorithm
     * @param array - not sorted array with numbers
     * @return - sorted array
     */
    @SortMethod(name = "Bubble ASC Sort")
    protected T[] ascBubbleSort(T[] array) {
        return sortArrayByBubble(array, SortDirection.ASC);
    }

    /**
     * bubble sort algorithm
     * @param array - not sorted array
     * @param direction - direction of sorting
     * @return - sorted array
     */
    private T[] sortArrayByBubble(T[] array, SortDirection direction) {

        for (int j = 0; j < array.length - 1; j++) {
            for (int i = 0; i < array.length - j - 1; i++) {
                if (direction == SortDirection.ASC) {
                    if (compare(array[i], array[i + 1]) > 0) {
                        T tmpElement = array[i];
                        array[i] = array[i + 1];
                        array[i + 1] = tmpElement;
                    }
                } else {
                    if (compare(array[i], array[i + 1]) < 0) {
                        T tmpElement = array[i];
                        array[i] = array[i + 1];
                        array[i + 1] = tmpElement;
                    }
                }
            }
        }

        return array;
    }

    /**
     * shaker sort algorithm
     * @param array - not sorted array
     * @return - sorted array
     */
    @SortMethod(name = "Shaker Sort")
    protected T[] shakerSort(T[] array) {
        int leftMark = 1;
        int rightMark = array.length - 1;
        while (leftMark <= rightMark) {
            for (int i = rightMark; i >= leftMark; i--)
                if (compare(array[i - 1], array[i]) > 0) {
                    T tmpElement = array[i - 1];
                    array[i - 1] = array[i];
                    array[i] = tmpElement;
                }
            leftMark++;

            for (int i = leftMark; i <= rightMark; i++)
                if (compare(array[i - 1], array[i]) > 0) {
                    T tmpElement = array[i - 1];
                    array[i - 1] = array[i];
                    array[i] = tmpElement;
                }
            rightMark--;

        }
        return array;
    }

    /**
     * Compare to
     * @param firstNum - first number to compare
     * @param secondNum - second number to compare
     * @return - the value 0 if firstNum is numerically equal to secondNum;
     *          a value less than 0 if firstNum is numerically less than secondNum;
     *          a value greater than 0 if firstNum is numerically greater than secondNum;
     *          and UnsupportedOperationException if type of firstNum not equal type for secondNum .
     */
    public int compare(T firstNum, T secondNum) {
        if (firstNum instanceof Comparable)
            if (firstNum.getClass().equals(secondNum.getClass()))
                return ((Comparable<T>) firstNum).compareTo(secondNum);
        throw new UnsupportedOperationException();
    }


    /**
     * Enum for sorting direction
     */
    public enum SortDirection {
        DESC, ASC;
    }

    /**
     * Get list of sorting method from this class and it super class
     * @param clazz - bottom class from with start search
     * @return - list methods with @SortMethod annotation
     */
    protected List<Method> getListSortMethods(Class<?> clazz) {
        List<Method> annotatedMethods = new ArrayList<Method>();

        while (clazz != null){
            Method[] methods = clazz.getDeclaredMethods();

            for (Method method : methods) {
                if (method.isAnnotationPresent(SortMethod.class)) {
                    annotatedMethods.add(method);
                }
            }
            clazz = clazz.getSuperclass();
        }
        return annotatedMethods;
    }
}
