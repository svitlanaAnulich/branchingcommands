package app.arrays.create;


import app.arrays.sort.MySorting;
import app.arrays.sort.Sorting;
import java.util.Random;

/**
 * Class witch create array with numbers
 */
public class FillArray {

    private Number[] array;
    private MySorting sorting = new MySorting();
    private Class arrayType;

    public FillArray(Integer sizeArray, Class arrayType) {
        this.array = new Number[sizeArray];
        this.arrayType = arrayType;
        fillArrayRandomNumbers();
    }

    /**
     * Sort elements of array with DESC direction
     * @return - array with DESC direction numbers
     */
    @ArrayFilling(name = "DESC sort array")
    public Number[] getDESCSortArray() {
        return sorting.arraysSort(array);
    }

    /**
     * Sort array with ASC direction and add some number to the end
     * @return - array
     */
    @ArrayFilling(name = "array with one number at the and")
    public Number[] getSortArrayWithOneNumber() {
        Number[] newArray = sorting.arraysSort(array);
        Number[] returnArray = new Number[array.length+1];
        System.arraycopy( newArray, 0, returnArray, 0, newArray.length);
        returnArray[array.length] = getRandomNumber(arrayType);
        return returnArray;
    }

    /**
     * Sort elements of array with ASC direction
     * @return - array with ASC direction numbers
     */
    @ArrayFilling(name = "ASC sort array")
    public Number[] getASCSortArray(){
        return  sorting.arraysSort(array);
    }

    /**
     * Array with random numbers
     * @return
     */
    @ArrayFilling(name = "random numbers array")
    public Number[] getRandomNumbersArray(){
        return array;
    }

    /**
     * Fill array by random numbers
     */
    private void fillArrayRandomNumbers() {
        for (int i = 0; i < array.length; i++) {
            array[i] = getRandomNumber(arrayType);
        }
    }

    /**
     * Get random number according to arrayType
     * @param arrayType - number type
     * @return - number
     */
    private Number getRandomNumber(Class arrayType) {
        Random random = new Random();
        Number number = null;
        if(arrayType.equals(Double.class)){
             number = random.nextDouble();
        } else if (arrayType.equals(Float.class)){
            number = random.nextFloat();
        } else if (arrayType.equals(Integer.class)){
            number = random.nextInt();
        } else if (arrayType.equals(Long.class)){
            number = random.nextLong();
        } else if (arrayType.equals(Byte.class)){
            number = (byte) random.nextInt();
        } else if (arrayType.equals(Short.class)){
            number = (short) random.nextInt();
        }
        return number;
    }

}
